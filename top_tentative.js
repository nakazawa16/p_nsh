$(document).ready(function($){


  $('.slide_ul').bxSlider({ //bxsliderを使用しているulクラスを指定
    easing: 'easeOutBounce',
    minSlides: 1,
    maxSlides: 1,
    slideWidth: 1000,
    slideMargin: 5,
    ticker: true,//tickerオプション、デフォルトはfalse
    speed: 65000
    });


  $('#SLIDE_BODY_ARCADE').click(function(){
    window.location.href = "./gamecenter.html";
  });
  $('#SLIDE_BODY_BOLDERING').click(function(){
  window.location.href = "./bouldering.html";
  })
  $('#SLIDE_BODY_BOWLING').click(function(){
    window.location.href = "./bowling.html";
  })
  $('#SLIDE_BODY_BILLIARDS').click(function(){
    window.location.href = "./billiards.html";
  })



  $('.slide_int').hover(
      function() {
          //マウスカーソルが重なった時の処理
          $(this).css('filter', 'brightness(50%)');

      },
      function() {
          //マウスカーソルが離れた時の処理
          $(this).css('filter', 'brightness(100%)');
      }
  );

});
